package com.example;

import static org.junit.Assert.assertThat;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MainTest {

	@LocalServerPort
	private int PORT;
	
	@Autowired
	private TestRestTemplate restTemmplate;
	
	@Test
	public void checkTitle() {
		String html = this.restTemmplate.getForObject("http://localhost:" + PORT + "/", String.class);
		assertThat("Check page Title", html, CoreMatchers.containsString("Moonlight Academy | CI/CD demo with Java + Spring Boot"));
	}
	
	@Test
	public void checkInfoBoxText() {
		String html = this.restTemmplate.getForObject("http://localhost:" + PORT + "/", String.class);
		assertThat("Check info box", html, CoreMatchers.containsString("This is a demonstration of CI/CD concept with Java + Spring Boot"));
	}
	
	@Test
	public void checkVersion() {
		String html = this.restTemmplate.getForObject("http://localhost:" + PORT + "/", String.class);
		String version = Main.VERSION;
		assertThat("App version should be same Main.VERSION", html, CoreMatchers.containsString(version));
	}

}